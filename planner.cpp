/*=================================================================
 *
 * planner.c
 *
 *=================================================================*/
#include "utils.h"
#include "MyPlanner.h"


static void planner(
				double*	map,
				int x_size,
				int y_size,
				double* armstart_anglesV_rad,
				double* armgoal_anglesV_rad,
				int numofDOFs,
				double*** plan,
				int* planlength,
				int* n_spl,
		 		int planner_id)
{
	srand(time(0));

	MyPlanner myplanner(map, x_size, y_size,
          armstart_anglesV_rad,
          armgoal_anglesV_rad,
          numofDOFs,
          planner_id);
  myplanner.plan(plan, planlength, n_spl);
  return;
}

//prhs contains input parameters (3):
//1st is matrix with all the obstacles
//2nd is a row vector of start angles for the arm
//3nd is a row vector of goal angles for the arm
//plhs should contain output parameters (2):
//1st is a 2D matrix plan when each plan[i][j] is the value of jth angle at the ith step of the plan
//(there are D DoF of the arm (that is, D angles). So, j can take values from 0 to D-1
//2nd is planlength (int)
void mexFunction( int nlhs, mxArray *plhs[], int nrhs, const mxArray*prhs[])
{

    /* Check for proper number of arguments */
    if (nrhs != 4) {
	    mexErrMsgIdAndTxt( "MATLAB:planner:invalidNumInputs",
                "Four input arguments required.");
    } else if (nlhs != 2) {
	    mexErrMsgIdAndTxt( "MATLAB:planner:maxlhs",
                "One output argument required.");
    }

    /* get the dimensions of the map and the map matrix itself*/
    int x_size = (int) mxGetM(MAP_IN);
    int y_size = (int) mxGetN(MAP_IN);
    double* map = mxGetPr(MAP_IN);

    /* get the start and goal angles*/
    int numofDOFs = (int) (MAX(mxGetM(ARMSTART_IN), mxGetN(ARMSTART_IN)));
    if(numofDOFs <= 1){
	    mexErrMsgIdAndTxt( "MATLAB:planner:invalidnumofdofs",
                "it should be at least 2");
    }
    double* armstart_anglesV_rad = mxGetPr(ARMSTART_IN);
    if (numofDOFs != MAX(mxGetM(ARMGOAL_IN), mxGetN(ARMGOAL_IN))){
        	    mexErrMsgIdAndTxt( "MATLAB:planner:invalidnumofdofs",
                "numofDOFs in startangles is different from goalangles");
    }
    double* armgoal_anglesV_rad = mxGetPr(ARMGOAL_IN);

    //get the planner id
    int planner_id = (int)*mxGetPr(PLANNER_ID_IN);
    if(planner_id < -1 || planner_id > 4){
	    mexErrMsgIdAndTxt( "MATLAB:planner:invalidplanner_id",
                "planner id should be between 0 and 3 inclusive");
    }

    //call the planner
    double** plan = NULL;
    int planlength = 0, n_spl = 0;

    //you can may be call the corresponding planner function here
    //if (planner_id == RRT)
    //{
    //    plannerRRT(map,x_size,y_size, armstart_anglesV_rad, armgoal_anglesV_rad, numofDOFs, &plan, &planlength);
    //}

    //dummy planner which only computes interpolated path
    planner(map,x_size,y_size, armstart_anglesV_rad, armgoal_anglesV_rad, numofDOFs, &plan, &planlength, &n_spl, planner_id);


    printf("planner returned plan of length=%d\n\n\n", planlength);

    /* Create return values */
    if(planlength > 0)
    {
        PLAN_OUT = mxCreateNumericMatrix( (mwSize)planlength, (mwSize)numofDOFs, mxDOUBLE_CLASS, mxREAL);
        double* plan_out = mxGetPr(PLAN_OUT);
        //copy the values
        int i,j;
        for(i = 0; i < planlength; i++)
        {
            for (j = 0; j < numofDOFs; j++)
            {
                plan_out[j*planlength + i] = plan[i][j];
            }
        }
    }
    else
    {
        PLAN_OUT = mxCreateNumericMatrix( (mwSize)1, (mwSize)numofDOFs, mxDOUBLE_CLASS, mxREAL);
        double* plan_out = mxGetPr(PLAN_OUT);
        //copy the values
        int j;
        for(j = 0; j < numofDOFs; j++)
        {
                plan_out[j] = armstart_anglesV_rad[j];
        }
    }
    PLANLENGTH_OUT = mxCreateNumericMatrix( (mwSize)1, (mwSize)1, mxINT16_CLASS, mxREAL);
    int* planlength_out = (int*) mxGetPr(PLANLENGTH_OUT);
    *planlength_out = n_spl;


    return;

}
